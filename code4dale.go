package main

import (
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/container/layout"
	"github.com/crazy-max/fyne-webview/v2"
)

func main() {
	myApp := app.New()
	myWindow := myApp.NewWindow("Fyne WebView with AdMob Example")

	// Create a WebView
	webview := webview.New("https://your-website-with-admob-ads.com", true)

	// Create a container with the WebView
	container := container.New(layout.NewGridLayout(1),
		webview,
	)

	// Set the window content
	myWindow.SetContent(container)

	// Show and run the application
	myWindow.ShowAndRun()
}
